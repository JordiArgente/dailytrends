Daily Trends Jordi Argente:

Recursos utilizados:
- Arquitectura y Framework desarrrollo: Laravel 5 
- Framework interfaz: Bootstrap
- Base de datos: MySQL

En la carpeta ra�z se adjunta:
- Diagrama arquitectura b�sica: "Arquitectura_Daily_Trends.png"
- Script creaci�n de tablas necesarias en MySQL: "daily_trends.sql"

Rutas de inter�s:
- Views: "\resources\views\*"
- Controllers: "\app\Http\Controllers\FeedController.php"
- Models: "\app\Feed.php"