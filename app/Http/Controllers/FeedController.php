<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Feed;
use SimpleXMLElement;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $otherFeeds = $this->getOtherFeeds();
        $feeds = Feed::all()->sortByDesc("id")->toArray();

        return view('pages.index', compact('feeds','otherFeeds'));
    }

    /**
     * Return list of feed of rss.
     *
     * @return $otherFeeds
     */
    private function getOtherFeeds() {
        $listRss = array();
        array_push($listRss, 'http://estaticos.elmundo.es/elmundo/rss/portada.xml');
        array_push($listRss, 'http://ep00.epimg.net/rss/tags/ultimas_noticias.xml');
        $otherFeeds = [];

        foreach ($listRss as $rss) {
            $content = file_get_contents($rss);
            $feedsXml = new SimpleXmlElement($content);

            $i = 0;
            foreach($feedsXml->channel->item as $entry) {
                if (++$i == 6) break;

                if($entry->children('http://purl.org/rss/1.0/modules/content/')->encoded){
                    $body = $entry->children('http://purl.org/rss/1.0/modules/content/')->encoded;
                } else {
                    $body = $entry->children("media", true)->description. "... <br><br><a href='".$entry->link."'>Seguir leyendo.</a>";
                }

                if($entry->enclosure){
                    $image = $entry->enclosure->attributes()->url;
                } else if ($entry->children('media', True)->content) {
                    $image = $entry->children('media', True)->content->attributes();
                } else {
                    $image = "http://www.cp67.com/imagenes/1494862477.gif";
                }

                $feed = new Feed([
                  'title' => $entry->title,
                  'body' => $body,
                  'image' => $image,
                  'source' => $entry->link,
                  'publisher' => $entry->children('http://purl.org/dc/elements/1.1/')->creator,
                ]);
                array_push($otherFeeds, $feed);
            }
        }

        return $otherFeeds;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         $feed = new Feed([
           'title' => $request->get('title'),
           'body' => $request->get('body'),
           'image' => $request->get('image'),
           'source' => $request->get('source'),
           'publisher' => $request->get('publisher'),
         ]);
         $feed->save();

         return redirect('/');
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feed = Feed::find($id);

        return view('pages.show', compact('feed','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feed = Feed::find($id);

        return view('pages.edit', compact('feed','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feed = Feed::find($id);
        $feed->title = $request->get('title');
        $feed->body = $request->get('body');
        $feed->image = $request->get('image');
        $feed->source = $request->get('source');
        $feed->publisher = $request->get('publisher');
        $feed->save();

        return view('pages.show', compact('feed', 'id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feed = Feed::find($id);
        $feed->delete();

        return redirect('/');
    }
}
