-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-03-2018 a las 22:01:35
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dailytrends`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `feeds`
--

CREATE TABLE `feeds` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `publisher` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `feeds`
--

INSERT INTO `feeds` (`id`, `title`, `body`, `image`, `source`, `publisher`, `created_at`, `updated_at`) VALUES
(1, 'La quiniela de los Oscar 2018', 'Ha llegado el momento. Tras estudiar cada categoría nos arriesgamos a adivinar el futuro, y a concretar los favoritos en cada categoría de los Oscar. No por calidad o gusto personal, sino por cómo ha ido la temporada de premios. Aquí va el listado:\r\n\r\nMejor película: La forma del agua, de Guillermo del Toro. ¿Por qué? Porque ganó los premios del Sindicato de Productores (aunque digamos sindicatos, en realidad deberíamos de calificarlos de gremios) y del Sindicato de Directores. Pero cuidado, hay muchísimas dudas porque Tres anuncios en las afueras ganó el premio a mejor reparto del Sindicato de Actores (el grupo profesional más numeroso entre los académicos de Hollywood), ganó en los Bafta, y el ruido sobre el posible plagio de Del Toro pueden arruinar la carrera del filme del mexicano. A esta apuesta se apunta Los Angeles Times, mientras que Indiewire, The Hollywood Reporter y Variety apoyan a Del Toro.\r\n\r\nMejor dirección: Guillermo del Toro, por La forma del agua. ¿Por qué? Porque se ha llevado todos los galardones en su apartado, incluido los del Sindicato de Directores y el Bafta. Y así el trío de mosqueteros mexicanos habrá logrado su oscar correspondiente en esta categoría.', 'https://ep01.epimg.net/cultura/imagenes/2018/03/02/actualidad/1520001619_020834_1520001716_noticia_normal_recorte1.jpg', 'https://elpais.com/cultura/2018/03/02/actualidad/1520001619_020834.html', 'Daily Trends', '2018-03-04 12:57:46', '2018-03-08 15:02:59');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `feeds`
--
ALTER TABLE `feeds`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `feeds`
--
ALTER TABLE `feeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
