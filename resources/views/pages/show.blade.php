<!-- show.blade.php -->
@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8" style="float: none; margin: 0 auto;">
            <!-- Title -->
            <h1 class="mt-4">{{$feed->title}}</h1>
            <!-- Image -->
            <img class="img-fluid rounded" src="{{$feed->image}}" alt=""><hr>
            <!-- Body -->
            <p>{!!nl2br($feed->body)!!}</p>
            <!-- Publisher & Source -->
            <hr>
  		      <p>Publisher: {{$feed->publisher}}</p>
            <p>Source: {{$feed->source}}</p>
            <hr style="border-top: 3px double #8c8b8b;">

            <form action="{{action('FeedController@destroy', $id)}}" method="post">
                {{csrf_field()}}
                <a href="{{action('FeedController@edit', $id)}}" class="btn btn-warning">Edit</a>
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-danger" type="submit">Delete</button>
            </form>
        </div>
    </div>
</div>
@endsection
