<!-- index.blade.php -->
@extends('master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8" style="float: none; margin: 0 auto;">
            <!-- Feeds Daily Trends -->
            @foreach($feeds as $post)
                <!-- Title -->
                <h1 class="mt-4"><a href="{{action('FeedController@show', $post['id'])}}">{{$post['title']}}</a></h1>
                <!-- Image -->
                <img class="img-fluid rounded" src="{{$post['image']}}" alt=""><hr>
                <!-- Body (limit text to 400 characters) -->
                {!!nl2br(str_limit(strip_tags($post['body']), 400))!!}
                @if (strlen(strip_tags($post['body'])) > 400)
                    <br><br><a href="{{action('FeedController@show', $post['id'])}}">Read More</a>
                @endif
                <!-- Publisher & Source -->
                <hr>
                <p>Publisher: {{$post['publisher']}}</p>
                <p>Source: {{$post['source']}}</p>
                <hr style="border-top: 3px double #8c8b8b;">
            @endforeach

            <!-- Feeds other newspapers -->
            @foreach ($otherFeeds as $post)
                <!-- Title -->
                <h1 class="mt-4">{{$post['title']}}</h1>
                <!-- Image -->
                <img class="img-fluid rounded" src="{{$post['image']}}" alt=""><hr>
                {!!nl2br($post['body'])!!}
                <!-- Publisher & Source -->
                <hr>
                <p>Publisher: {{$post['publisher']}}</p>
                <p>Source: {{$post['source']}}</p>
                <hr style="border-top: 3px double #8c8b8b;">
            @endforeach
        </div>
    </div>
</div>
@endsection
